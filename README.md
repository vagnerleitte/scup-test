# Test

### Introdução
Para executar a aplicação siga os passos abaixo.

  - instalar as dependencias do npm
  - instalar as dependencias do bower
  - compilar os arquivos less
  - rodar a aplicação

### Instalação

Para instalar as dependencias do npm basta navegar até o diretório da aplicação pelo terminal e digitar o comando:
```sh
$ npm install
```

Após instalar todas as dependencias npm da aplicação você deve instalar as dependencias fornecidas pelo bower com o seguinte comando:
```sh
$ bower install
```

Vamos agora compilar nosso less para gerar o css necessário para nossa aplicação com o seguinte comando:

```sh
$ gulp
```

### Executando a aplicação

Feito isso já podemos inicializar nossa aplicação digitando o comando:
```sh
$ npm run dev
```

### Testes
Para rodar os testes automatizados é só utilizar o seguinte comando:

```sh
$ npm run test
```