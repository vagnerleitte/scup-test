module.exports = {
    entry: './src/js/app.js',
    watch: true,
    output: {
        path: './public',
        filename: 'bundle.js',
    },
    module: {
        loaders: [{
            test: /\.js$/,
            exclude: /(node_modules|public|bower_components)/,
            loader: 'babel-loader'
        }]
    },
    resolve: {
        extensions: ['', '.js', '.json']
    }
};
