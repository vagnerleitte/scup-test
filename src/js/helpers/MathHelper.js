const MathHelper = {
    getDecimalOnly: function(num) {
        num = (num + "").split(".");

            if( num[1] === undefined) {
                return "00";
            } else if( num[1].length == 1 ){
                return num[1] + "0";
            }
        
        return num[1].toString().substring(0, 2);
    }
};

export default MathHelper;