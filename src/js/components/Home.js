import React from 'react';
import ReactDOM from 'react-dom';

import Header from './Header';
import Content from './Content';
import Footer from './Footer';
import Modal from './Modal';
import fectch from 'whatwg-fetch/fetch';


const Home = React.createClass({
    getInitialState: function() {
        return {
            modalIsOpen: false,
            modalTitle: "Selecione",
            field: null,
            fieldOrigin: "",
            fieldDestination: "",
            fieldMinutes: "",
            ddds: {},
            plans: {},
            pricing: {}
        }
    },
    toggleModal: function(open) {
        this.setState({
            modalIsOpen: open
        });
    },
    setModalTitle: function(title) {
        this.setState({
            modalTitle: title
        });
    },
    setFieldID: function(id) {
        this.setState({
            field: id
        });
    },
    setField: function(ddd, field) {

        switch(field) {
            case "origin-field":
                this.setState({
                    fieldOrigin:  ddd
                });
                return;
            case "destination-field":
                this.setState({
                    fieldDestination:  ddd
                });
                break;
            case "minutes-field":
                this.setState({
                    fieldMinutes:  ddd
                });
                break;
            default:
                return;
        }
    },
    getPricingFromServer: function() {
        var _this = this;
        var f = fetch('http://private-fe2a-scuptel.apiary-mock.com/ddd/pricing')
        .then(function(res) {
            return res.text();
        }).then(function(body) {
            _this.setState({pricing: JSON.parse(body)});
        });

    },
    getDDDsFromServer: function() {
        var _this = this;
        var f = fetch('http://private-fe2a-scuptel.apiary-mock.com/ddd/details')
        .then(function(res) {
            return res.text();
        }).then(function(body) {
            _this.setState({ddds: JSON.parse(body)});
        });

    },
    getPlansFromServer: function() {
        var _this = this;
        var f = fetch('http://private-fe2a-scuptel.apiary-mock.com/plans')
        .then(function(res) {
            return res.text();
        }).then(function(body) {
            _this.setState({plans: JSON.parse(body)});
        });

    },
    componentDidMount: function() {
        this.getPricingFromServer();
        this.getDDDsFromServer();
        this.getPlansFromServer()

    },
    render: function() {

        return (
            <div>
                <div className="main-content">
                    <Header/>
                    <Content
                        toggleModal={this.toggleModal}
                        setModalTitle={this.setModalTitle}
                        setFieldID={this.setFieldID}
                        setMinutes={this.setField}
                        fieldOrigin={this.state.fieldOrigin}
                        fieldDestination={this.state.fieldDestination}
                        fieldMinutes={this.state.fieldMinutes}
                        plans={this.state.plans.data}
                        totalPlans={this.state.plans.total}
                        pricing={this.state.pricing.data}
                        />
                    <Footer/>
                </div>
                <Modal
                    modalIsOpen={this.state.modalIsOpen}
                    toggleModal={this.toggleModal}
                    modalTitle={this.state.modalTitle}
                    field={this.state.field}
                    ddds={this.state.ddds.data}
                    setField={this.setField}
                     />
            </div>
        );
    }
});

export default Home;