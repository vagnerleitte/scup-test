import React from 'react';
import ReactDOM from 'react-dom';
import Plan from './Plan';

const Results = React.createClass({
    getDefaultProps: function() {
        return {
            plans: {}
        }
    },
    appendNoPlan: function(plans) {
        plans.push({ plan: "Normal", time: 0});
    },
    render: function() {
        
        if (this.props.plans.length == undefined) {
            return null;
        }

        var _this = this;
        var rows = [];

        if(this.props.plans.length <= this.props.totalPlans){
            this.appendNoPlan(this.props.plans);
        }

        this.props.plans.forEach(function(plan, i){
            rows.push(<Plan 
                        key={i} 
                        plan={plan}
                        pricing={_this.props.pricing} 
                        setMinutes={_this.props.setMinutes}
                        fieldOrigin={_this.props.fieldOrigin}
                        fieldDestination={_this.props.fieldDestination}
                        fieldMinutes={_this.props.fieldMinutes}
                        pricing={_this.props.pricing}
                        />
                    );
        });
        return (
            <div className="results">
                {rows}
            </div>
            );
    }
});
    
export default Results;