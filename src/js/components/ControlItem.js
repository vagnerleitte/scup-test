import React from 'react';
import ReactDOM from 'react-dom';

const ControlItem = React.createClass({
    getDefaultProps: function() {
        return {
            controlID: "control",
            controlName: "control",
            controlClassName: "",
            controlTitle: "Choose or input one option",
            controlLabel: "Label for control",
            controlPlaceholder: "Placeholder",
            showButton: false,
            controlButtonLabel: "button",
            readOnly: false,
            onClickControl: function(){},
            controlValue: "",
            onChangeControl: function() {}

        }  
    },
    onClickControl: function(event) {
        this.props.onClickControl(this.props.controlLabel, this.props.controlID);
    },
    onChangeControl: function(event) {
        this.props.onChangeControl(event.target.value, this.props.controlID);
    },
    render: function(){
        var opts = {};
        if( this.props.readOnly === true ) {
            opts['readOnly'] = 'readOnly';
            opts['value'] = this.props.controlValue;
        } else {
            opts['onChange'] = this.onChangeControl;
        }

        return (
            <div className={"input-control " + this.props.controlClassName}>
                <label htmlFor={this.props.controlID}>
                    { this.props.controlLabel }
                </label>
                <div className="input-box">
                    <input 
                        type="text"
                        title={this.props.controlTitle} 
                        placeholder={this.props.controlPlaceholder}
                        {...opts}
                        id={this.props.controlID}
                        name={this.props.controlName} 
                        maxLength="3"
                        onClick={this.onClickControl}
                        />
                    { this.props.showButton ? 
                        <span 
                            className="input-button"
                            > { this.props.controlButtonLabel } </span>
                        :
                        ""
                    }
                </div>
            </div>
            );     
    }
});

export default ControlItem;
