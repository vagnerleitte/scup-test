import React from 'react';
import ReactDOM from 'react-dom';
import Controls from './Controls';
import Results from './Results';

const Content = React.createClass({
    getDefaultProps: function() {
        return {
            toggleModal: function() {},
            setModalTitle: function() {},
            setFieldID: function() {},
            setMinutes: function() {},
            fieldOrigin: "",
            fieldDestination: "",
            fieldMinutes:"",
            plans: {},
            totalPlans: 0
        }
    },
    render: function() {
        return (
            <section className="content">
                <Controls 
                    toggleModal={this.props.toggleModal}
                    setModalTitle={this.props.setModalTitle}
                    setFieldID={this.props.setFieldID}
                    setMinutes={this.props.setMinutes}
                    fieldOrigin={this.props.fieldOrigin}
                    fieldDestination={this.props.fieldDestination}
                    />
            
                <Results 
                    plans={this.props.plans} 
                    pricing={this.props.pricing} 
                    setMinutes={this.props.setMinutes}
                    fieldOrigin={this.props.fieldOrigin}
                    fieldDestination={this.props.fieldDestination}
                    fieldMinutes={this.props.fieldMinutes}
                    totalPlans={this.props.totalPlans}
                    />
                
            </section>
            );
    }
});

export default Content;