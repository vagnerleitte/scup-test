import React from 'react';
import ReactDOM from 'react-dom';
import MathHelper from '../helpers/MathHelper';

const Plan = React.createClass({
    getInitialState: function() {
        return {
            price: "-"
        };
    },
    getDefaultProps: function() {
        return {
            fieldOrigin: "",
            fieldDestination: "",
            fieldMinutes: ""
        };
    },
    validateOrigin: function(nextProps) {
        return this.props.fieldOrigin != nextProps.fieldOrigin 
            || nextProps.fieldOrigin != "";
    },
    validateDestination: function(nextProps) {
        return this.props.fieldDestination != nextProps.fieldDestination 
            || nextProps.fieldDestination != "";
    },
    validateMinutes: function(nextProps) {
        return this.props.fieldMinutes != nextProps.fieldMinutes 
            || nextProps.fieldMinutes != ""
            || nextProps.fieldMinutes > 0
            ;
    },
    componentWillReceiveProps: function(nextProps) {

        if (this.props != nextProps) {
            if (this.validateOrigin(nextProps)
                && this.validateDestination(nextProps)
                && this.validateMinutes(nextProps)
                ) {
                var pricing = this.calculatePricing(nextProps);
            }
        }

    },
    calculatePricing(nextProps){
        var _this = this;
        var reset = true;
        _this.props.pricing.forEach(function(combination){
            if (combination.origin == nextProps.fieldOrigin 
                && combination.destiny == nextProps.fieldDestination) {
                reset = false;

                var _price = _this.calculate(combination.price,  nextProps.plan.time, nextProps.fieldMinutes);

                _this.setState({price: _price});
                
                return;
            }
        });

        if (reset || nextProps.fieldMinutes == "") {
            this.setState({price: "-"});
        }
    },
    calculate: function(planPrice, planMinutes, minutesChosen) {
        
        if (parseInt(minutesChosen, 10) <= parseInt(planMinutes, 10)){
            
            return 0;
        }

        var total =  (planPrice * (minutesChosen - planMinutes));

        return total + (total * 0.1);
    },
    render: function() {
        return (
            <div className="plan">
                <p>{this.props.plan.plan}</p>
                <p>
                    { this.state.price != "-" ?
                    <span className="value">
                        <small className="money">R$</small>{Math.floor(this.state.price).toFixed()},
                        <small>{MathHelper.getDecimalOnly(this.state.price)}</small>
                    </span> :
                    <span className="value">
                        -
                    </span>
                    }
                </p>
            </div>
            );
    }
});
    
export default Plan;
