import React from 'react';
import ReactDOM from 'react-dom';
import ControlItem from './ControlItem';

const Controls = React.createClass({
    getDefaultProps: function() {
        return {
            openModal: function() {},
            setModalTitle: function() {},
            setFieldID: function() {},
            setMinutes: function() {},
            fieldOrigin: "",
            fieldDestination: ""
        }
    },
    onClickControl: function(title, id){
        this.props.toggleModal(true);
        this.props.setModalTitle(title);
        this.props.setFieldID(id);
    },
    onChangeControl: function(value, id){
        this.props.setMinutes(value, id);
    },
    render: function () {
        return (
            <div className="controls">
                <h2>Comparar Planos</h2>
                <ControlItem 
                    controlID="origin-field"
                    controlName="origin-field"
                    controlClassName="origin"
                    controlTitle="Clique no botão ao lado para selecionar a origem."
                    controlLabel="Origem"
                    controlPlaceholder="DDD"
                    readOnly={true}
                    controlValue={this.props.fieldOrigin}
                    showButton={true}
                    controlButtonLabel="+"
                    value={this.props.fieldOrigin}
                    onClickControl={this.onClickControl}
                />

                <ControlItem 
                    controlID="destination-field"
                    controlName="destination-field"
                    controlClassName="destination"
                    controlTitle="Clique no botão ao lado para selecionar o destino."
                    controlLabel="Destino"
                    controlPlaceholder="DDD"
                    readOnly={true}
                    showButton={true}
                    controlValue={this.props.fieldDestination}
                    controlButtonLabel="+"
                    onClickControl={this.onClickControl}
                />

                <ControlItem 
                    controlID="minutes-field"
                    controlName="minutes-field"
                    controlClassName="minutes"
                    controlTitle="Informe a quantidade de minutos desejado."
                    controlLabel="Minutos"
                    controlPlaceholder="MM"
                    readOnly={false}
                    showButton={false}
                    onChangeControl={this.onChangeControl}
                />
            </div>
            );
    }
});

export default Controls;