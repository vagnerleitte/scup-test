import React from 'react';
import ReactDOM from 'react-dom';

const ItemDDD = React.createClass({
    getDefaultProps: function (argument) {
        return {
            data: {},
            itemClicked: function() {}
        }
    },
    handleClick: function(event) {
        event.preventDefault();
        this.props.itemClicked(this.props.data.ddd);
    },
    render: function() {
        return (
            <li>
                <a href="#" onClick={this.handleClick}>{this.props.data.ddd} - {this.props.data.city}</a>
            </li>
        );
    }
});

export default ItemDDD;