import React from 'react';
import ReactDOM from 'react-dom';

const Header = React.createClass({
    render: function() {
        return (
            <header>
                <h1><a href="/" className="logo">scup Fale +</a></h1>
            </header>
            );
    }
});

export default Header;