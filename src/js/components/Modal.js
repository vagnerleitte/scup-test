import React from 'react';
import ReactDOM from 'react-dom';
import ItemDDD from './ItemDDD';

const Modal = React.createClass({
    getDefaultProps: function() {
        return {
            ddds: {},
            modalIsOpen: false,
            toggleModal: function() {},
            modalTitle: "Selecione",
            setField: function() {},
            field: null
        }
    },
    getInitialState: function() {
        return {
            modalOpenClass: ""
        }
    },
    componentWillReceiveProps: function(nextProps) {
        if(this.props.modalIsOpen != nextProps.modalIsOpen){
            this.setState({
                modalOpenClass: nextProps.modalIsOpen ? " md-show" : ""
            })
        }
    },
    closeModal: function() {
        this.props.toggleModal(false);
    },
    itemClicked: function(ddd) {
        this.closeModal();
        this.props.setField(ddd, this.props.field);
    },
    render: function(argument) {

        if (this.props.ddds.length == undefined) {
            return null;
        }

        var rows = [];

        var itemClicked = this.itemClicked;

        this.props.ddds.map(function(item, i){
            rows.push(<ItemDDD data={item} key={i} itemClicked={itemClicked}/>);
        });
        return (
            <div className="modal">
                <div className={"md-modal md-effect-just-me" + this.state.modalOpenClass } id="modal-just-me">
                    <div className="md-content">
                        <h3>{this.props.modalTitle}</h3>
                        <div>
                            <ul>
                                {rows}
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="md-overlay" onClick={this.closeModal}></div>
            </div>
            );
    }
});

export default Modal;