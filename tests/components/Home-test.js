import React from 'react';
import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import Home from '../../src/js/components/Home';
import Header from '../../src/js/components/Header';
import Footer from '../../src/js/components/Footer';
import Modal from '../../src/js/components/Modal';

function mockItem(overrides){
}

describe('<Home />', () =>{

	it("Deve conter o elemento <Header />", function() {
		expect(shallow(<Home />).contains(<Header />)).to.equal(true);
	});
	it("Deve conter o elemento <Footer />", function() {
		expect(shallow(<Home />).contains(<Footer />)).to.equal(true);
	});

	it("Deve conter o elemento <Modal />", function() {
		expect(shallow(<Home />).find(Modal)).to.have.length(1);
	});
});
