import React from 'react';
import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import Content from '../../src/js/components/Content';
import Controls from '../../src/js/components/Controls';
import Results from '../../src/js/components/Results';

function mockItem(overrides){
}

describe('<Content />', () =>{

	let _toggleModal = false;
	let _setModalTitle = () => {};
	let _setFieldID = () => {};
	let _setMinutes = () => {};
	let _plans = mockItem({
				    "total": 3,
				    "data": [
				        {
				            "plan": "FaleMais 30",
				            "time": "30"
				        },
				        {
				            "plan": "FaleMais 60",
				            "time": "60"
				        },
				        {
				            "plan": "FaleMais 120",
				            "time": "120"
				        }
				    ]
				});
	let _price = mockItem({
				    "total": 6,
				    "data": [
				        {
				            "origin": "011",
				            "destiny": "016",
				            "price": "1.90"
				        },
				        {
				            "origin": "016",
				            "destiny": "011",
				            "price": "2.90"
				        },
				        {
				            "origin": "011",
				            "destiny": "017",
				            "price": "1.70"
				        },
				        {
				            "origin": "017",
				            "destiny": "011",
				            "price": "2.70"
				        },
				        {
				            "origin": "011",
				            "destiny": "018",
				            "price": "0.90"
				        },
				        {
				            "origin": "018",
				            "destiny": "011",
				            "price": "1.90"
				        }
				    ]
				});

	let content = <Content
		                        toggleModal={_toggleModal}
		                        setModalTitle={_setModalTitle}
		                        setFieldID={_setFieldID}
		                        setMinutes={_setMinutes}
		                        fieldOrigin="011"
		                        fieldDestination="017"
		                        fieldMinutes="120"
		                        plans={_plans}
		                        totalPlans={2}
		                        pricing={_price}/>;


	it("Deve conter o elemento <Content /> com a classe content", function() {
		expect(shallow(content).is('.content')).to.equal(true);
	});

	it("Deve conter 1 elemento Controls", function() {
	    expect(shallow(content).find(Controls)).to.have.length(1);
	});

	it("Deve conter 1 elemento Results", function() {
	    expect(shallow(content).find(Results)).to.have.length(1);
	});

	it("Deve conter a propriedade fieldOrigin com valor 011", function() {
		expect(mount(content).prop('fieldOrigin')).to.equal('011');
	});

	it("Deve conter a propriedade fieldDestination com valor 017", function() {
		expect(mount(content).prop('fieldDestination')).to.equal('017');
	});

	it("Deve conter a propriedade totalPlans com valor 2", function() {
		expect(mount(content).prop('totalPlans')).to.equal(2);
	});

});