var gulp = require('gulp');
var less = require('gulp-less');
var path = require('path');
var cssmin = require('gulp-cssmin');

gulp.task('less', function () {
  return gulp.src('./src/less/styles.less')
    .pipe(less({
      paths: [ path.join(__dirname, 'less', 'includes') ]
    }))
    .pipe(cssmin().on('error', function(err) {
        console.log(err);
    }))
    .pipe(gulp.dest('./public/css'));
});

gulp.task('watch', function() {
    gulp.watch('./src/less/**/*.less', ['less']);
});

gulp.task('default', ['less']);